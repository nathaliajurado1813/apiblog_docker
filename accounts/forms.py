# accounts/forms.py
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser

#Define un formulario personalizado para la creación de usuarios heredando de UserCreationForm
class CustomUserCreationForm(UserCreationForm):
    #añale los metadatos del formulario a la clase
    class Meta(UserCreationForm):
        model = CustomUser
        #incluye los campos del formulario heredado y le adiciona el campo name
        fields = UserCreationForm.Meta.fields + ("name",)

#Define un forumlario para editas los usuarios
class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        #muestra toda la información del formulario
        fields = UserChangeForm.Meta.fields