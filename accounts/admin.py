# accounts/admin.py
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser

#hereda de UserAdmin y permite la personalizar la administración del modelo 
class CustomUserAdmin(UserAdmin):
    #trae el formulario creado en forms.py
    add_form = CustomUserCreationForm
    # trae el formulario de actulización de datos que creamos en form
    form = CustomUserChangeForm
    #trae el modelo credo
    model = CustomUser
    #lo que se va a mostrar 
    list_display = [
        "email",
        "username",
        "name",
        "is_staff",
    ]
    #trae los campos de Useradmin y le añade el campo name cuando se edita un usuario
    fieldsets = UserAdmin.fieldsets + ((None, {"fields": ("name",)}),)
    #trae los campos para añadir un nuevo usuario y adiciona el campo name cuando se crea un usuario
    add_fieldsets = UserAdmin.add_fieldsets + ((None, {"fields": ("name",)}),)

#Registra el modelo en el sitio de administración de Django
admin.site.register(CustomUser, CustomUserAdmin)